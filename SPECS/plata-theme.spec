Name:           plata-theme
Version:        0.8.9
Release:        1%{?dist}
Summary:        A Gtk theme based on Material Design Refresh.

License:        GPLv2+ and CC_BY_SA4
URL:            https://gitlab.com/tista500/%{name}
Source0:        https://gitlab.com/tista500/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  inkscape
BuildRequires:  gdk-pixbuf2-devel
BuildRequires:  glib2-devel
BuildRequires:  libxml2-devel
BuildRequires:  pkgconf-pkg-config
BuildRequires:  parallel
BuildRequires:  sassc
BuildRequires:  marco-devel
Requires:       gtk-murrine-engine
Requires:       gtk2-engines

%description
A Gtk theme based on Material Design Refresh.

%prep
%autosetup


%build
NOCONFIGURE=1 ./autogen.sh
%configure --enable-mate \
           --enable-parallel \
           --enable-gtk_next \
           --enable-plank \
           --enable-telegram \
           --enable-tweetdeck
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install
install -d %{buildroot}/usr/share/plank/themes
ln -s /usr/share/themes/Plata/plank %{buildroot}/usr/share/plank/themes/Plata

%files
%{_datadir}/themes/Plata*
%{_datadir}/plank/themes/Plata
%license LICENSE_CC_BY_SA4



%changelog
* Sun Aug 18 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Update to 0.8.9

* Wed May  29 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Update to 0.8.3

* Wed May  22 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Update to 0.8.1

* Mon Apr  8 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial package
