Name:           otcl
Version:        1.14
Release:        6%{?dist}
Summary:        OTcl, short for MIT Object Tcl, is an extension to Tcl/Tk for object-oriented programming

License:        BSD
URL:            https://sourceforge.net/projects/otcl-tclcl/
Source0:        https://sourceforge.net/projects/otcl-tclcl/files/OTcl/%{version}/%{name}-src-%{version}.tar.gz
Patch0:		https://gitlab.com/ThanosApostolou/fedora-specs/raw/master/SOURCES/otcl-1.14-tcl86-compat.patch

BuildRequires:	gcc
BuildRequires:	libX11-devel
BuildRequires:	libXt-devel
BuildRequires:	tcl-devel
BuildRequires:	tk-devel

%description 
OTcl, short for MIT Object Tcl, is an extension to Tcl/Tk for object-oriented programming. It shouldn't be confused with the IXI Object Tcl extension by Dean Sheenan. (Sorry, but we both like the name and have been using it for a while.)

%prep
%setup
%patch0 -p1

%build
./configure --enable-static
make -j1
make -j1

%install
install -Dm755 otclsh %{buildroot}/%{_bindir}/otclsh
install -Dm755 owish %{buildroot}/%{_bindir}/owish
install -Dm644 otcl.h %{buildroot}/%{_includedir}/otcl.h
install -Dm644 libotcl.a %{buildroot}/%{_libdir}/libotcl.a
install -Dm755 libotcl.so %{buildroot}/%{_libdir}/libotcl.so
if test "%{_arch}" == x86_64; then
	install -d %{buildroot}/%{_prefix}/lib
	cd  %{buildroot}/%{_prefix}/lib
	ln -s ../lib64/libotcl.a libotcl.a
	ln -s ../lib64/libotcl.so libotcl.so
fi

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%{_bindir}/otclsh
%{_bindir}/owish
%{_includedir}/otcl.h
%{_libdir}/libotcl.a
%{_libdir}/libotcl.so
%{_prefix}/lib/libotcl.a
%{_prefix}/lib/libotcl.so

%changelog
* Sat Jun 03 2017 - 1.14-5
- rebuild

* Fri May 05 2017 - 1.14-4
- Initial version of the package
