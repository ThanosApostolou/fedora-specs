Name:           obsidian-icon-theme
Version:        4.7
Release:        1%{?dist}
Summary:        Gnome Icon Pack based upon Faenza

License:        GPLv3+
URL:            https://github.com/madmaxms/iconpack-obsidian
Source0:        https://github.com/madmaxms/iconpack-obsidian/archive/v%{version}.tar.gz

BuildArch:      noarch

%description
Gnome Icon Pack based upon Faenza

%prep
%autosetup -n iconpack-obsidian-%{version}


%install
rm -rf $RPM_BUILD_ROOT
install -d %{buildroot}/%{_datadir}/icons
cd %{_builddir}/%{buildsubdir}
for D in Obsidian*
do
    mv $D %{buildroot}/%{_datadir}/icons
done

%post
for i in %{_datadir}/icons/Obsidian*
do
	/bin/touch --no-create $i &>/dev/null || :
done

%postun
for i in %{_datadir}/icons/Obsidian*
do
	/bin/touch --no-create $i &>/dev/null
	gtk-update-icon-cache -q $i &>/dev/null
done

%posttrans
for i in %{_datadir}/icons/Obsidian*
do
	gtk-update-icon-cache -q $i &>/dev/null
done

%files
%license LICENSE
%{_datadir}/icons/Obsidian*


%changelog
* Tue May  21 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Update to 4.7 add post scriplets

* Wed Apr 10 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial package
