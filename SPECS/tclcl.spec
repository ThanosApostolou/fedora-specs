%global debug_package %{nil}

Name:           tclcl
Version:        1.20
Release:        4%{?dist}
Summary:        clCL is a Tcl/C++ interface used by Mash, vic, vat, rtp_play, ns, and nam

License:        BSD
URL:            https://sourceforge.net/projects/otcl-tclcl/
Source0:        https://sourceforge.net/projects/otcl-tclcl/files/TclCL/%{version}/%{name}-src-%{version}.tar.gz
Patch0:		https://gitlab.com/ThanosApostolou/fedora-specs/raw/master/SOURCES/tclcl-1.20-tcl86-compat.patch

Requires:	otcl
BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	libX11-devel
BuildRequires:	libXt-devel
BuildRequires:	tcl-devel
BuildRequires:	tk-devel
BuildRequires:	otcl
BuildRequires:	libstdc++-static
BuildRequires:	glibc-static

%description 
TclCL (Tcl with classes) is a Tcl/C++ interface used by Mash, vic, vat, rtp_play, ns, and nam. It provides a layer of C++ glue over OTcl

%prep
%setup
%patch0 -p1

%build
./configure --enable-static
%make_build -j1

%install
install -Dm755 tcl2c++ %{buildroot}/%{_bindir}/tcl2c++
install -Dm644 config.h %{buildroot}/%{_includedir}/config.h
install -Dm644 idlecallback.h %{buildroot}/%{_includedir}/idlecallback.h
install -Dm644 iohandler.h %{buildroot}/%{_includedir}/iohandler.h
install -Dm644 rate-variable.h %{buildroot}/%{_includedir}/rate-variable.h
install -Dm644 tclcl-config.h %{buildroot}/%{_includedir}/tclcl-config.h
install -Dm644 tclcl-internal.h %{buildroot}/%{_includedir}/tclcl-internal.h
install -Dm644 tclcl-mappings.h %{buildroot}/%{_includedir}/tclcl-mappings.h
install -Dm644 tclcl.h %{buildroot}/%{_includedir}/tclcl.h
install -Dm644 timer.h %{buildroot}/%{_includedir}/timer.h
install -Dm644 tracedvar.h %{buildroot}/%{_includedir}/tracedvar.h
install -Dm644 libtclcl.a %{buildroot}/%{_libdir}/libtclcl.a
if test "%{_arch}" == x86_64; then
	install -d %{buildroot}/%{_prefix}/lib
	cd  %{buildroot}/%{_prefix}/lib
	ln -s ../lib64/libtclcl.a libtclcl.a
fi

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%{_bindir}/tcl2c++
%{_includedir}/*.h
%{_libdir}/libtclcl.a
%{_prefix}/lib/libtclcl.a

%changelog
* Sat Jun 03 2017 - 1.20-4
- Rebuild

* Fri May 05 2017 - 1.20-3
- Initial version of the package
