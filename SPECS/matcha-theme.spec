Name:           matcha-theme
Version:        20190701
Release:        1%{?dist}
Summary:        a flat Design theme for GTK 3, GTK 2 and Gnome-Shell

License:        GPLv3+
URL:            https://github.com/vinceliuice/matcha
Source0:        https://github.com/vinceliuice/matcha/archive/v2019-07.tar.gz

BuildArch:      noarch

Requires:       gtk-murrine-engine
Requires:       gtk2-engines

%description
Matcha is a flat Design theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3
and GTK 2 based desktop environments like Gnome, Unity, Budgie, Pantheon, XFCE, Mate, etc.

%prep
%autosetup -n matcha-2019-07

%install
rm -rf $RPM_BUILD_ROOT
install -d %{buildroot}/%{_datadir}/themes
./Install -d %{buildroot}/%{_datadir}/themes


%files
%license LICENSE
%{_datadir}/themes/Matcha*


%changelog
* Thu Apr  18 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Change name to matcha-theme

* Sun Apr  7 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial package
