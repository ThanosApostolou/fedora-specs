Name:           materia-theme
Version:        20190315
Release:        1%{?dist}
Summary:        A Gtk theme based on Material Design Refresh.

License:        GPLv2+ and CC_BY_SA4
URL:            https://github.com/nana-4/%{name}
Source0:        https://github.com/nana-4/%{name}/archive/v%{version}.tar.gz
BuildArch:      noarch

Requires:       gnome-themes
Requires:       gtk-murrine-engine
Requires:       gtk2-engines

%description
a Material Design theme for GNOME/GTK based desktop environments.

%prep
%autosetup

%install
rm -rf $RPM_BUILD_ROOT
install -d "%{buildroot}/usr/share/themes"
./install.sh -d "%{buildroot}/usr/share/themes"

%files
%{_datadir}/themes/Materia*
%license COPYING

%changelog
* Sun Aug 18 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial inclusion

