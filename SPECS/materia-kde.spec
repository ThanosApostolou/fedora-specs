Name:           materia-kde
Version:        20190707
Release:        1%{?dist}
Summary:        port of the popular GTK theme Materia for Plasma 5 desktop

License:        GPLv2+ and CC_BY_SA4
URL:            https://github.com/PapirusDevelopmentTeam/%{name}
Source0:        https://github.com/PapirusDevelopmentTeam/%{name}/archive/%{version}.tar.gz
BuildArch:      noarch

%description
a port of the popular GTK theme Materia for Plasma 5 desktop with a few additions and extras

%prep
%autosetup

%install
rm -rf $RPM_BUILD_ROOT
install -d "%{buildroot}/usr/share"
cp -r plasma "%{buildroot}"/usr/share
cp -r aurorae "%{buildroot}"/usr/share
cp -r color-schemes "%{buildroot}"/usr/share
cp -r konsole "%{buildroot}"/usr/share
cp -r yakuake "%{buildroot}"/usr/share
cp -r Kvantum "%{buildroot}"/usr/share

%files
%{_datadir}/plasma
%{_datadir}/aurorae
%{_datadir}/color-schemes
%{_datadir}/konsole
%{_datadir}/yakuake
%{_datadir}/Kvantum
%license LICENSE

%changelog
* Sun Aug 18 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial inclusion

