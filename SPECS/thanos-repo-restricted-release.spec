Name:           thanos-repo-restricted-release
Version:        30
Release:        1%{?dist}
Summary:        Thanos Restricted Repository files
License:        GPLv3
URL:            https://gitlab.com/ThanosApostolou/thanos-repo-restricted
Source0:        https://gitlab.com/ThanosApostolou/thanos-repo-restricted/raw/master/thanos-repo-restricted.repo

BuildArch:      noarch

Requires:       filesystem

%description 
Thanos Restricted Repository files for yum and dnf

%install
install -Dm644 %{SOURCE0} %{buildroot}/%{_sysconfdir}/yum.repos.d/thanos-repo-restricted.repo

%files
%{_sysconfdir}/yum.repos.d/thanos-repo-restricted.repo

%changelog
* Thu Apr 18 2019 Thanos Apostolou - 30-1
- Initial package

