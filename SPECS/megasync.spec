Name:           megasync
Version:        4.1.1.0
Release:        1%{?dist}
Summary:        Easy automated syncing between your computers and your MEGA cloud drive.

License:        MEGA-LIMITED
URL:            https://mega.nz/
Source0:        https://github.com/meganz/MEGAsync/archive/v%{version}_Linux.tar.gz
Source1:        https://github.com/meganz/sdk/archive/v3.5.3.tar.gz

BuildRequires: openssl-devel, autoconf, automake, libtool, gcc-c++
BuildRequires: hicolor-icon-theme, unzip, wget, sed
BuildRequires: pkgconf-pkg-config
BuildRequires: LibRaw-devel
BuildRequires: desktop-file-utils
BuildRequires: qt5-qtbase-devel qt5-qttools-devel, qt5-qtsvg-devel
BuildRequires: terminus-fonts, fontpackages-filesystem
BuildRequires: bzip2-devel
BuildRequires: cryptopp-devel
BuildRequires: libsodium-devel
BuildRequires: zlib-devel
BuildRequires: sqlite-devel
BuildRequires: c-ares-devel
BuildRequires: libcurl-devel
BuildRequires: libuv-devel
BuildRequires: libzen-devel
BuildRequires: libmediainfo-devel
BuildRequires: ffmpeg-devel

# for extensions
BuildRequires: nautilus-devel
BuildRequires: nemo-devel

Requires: cryptopp >= 5.6.5
Requires: qt5-qtbase >= 5.6, qt5-qtsvg

%description
Easy automated syncing between your computers and your MEGA cloud drive.

%prep
%setup -a1 -q -n MEGAsync-%{version}_Linux
rm -rf ./src/MEGASync/mega/
mv sdk-3.5.3 ./src/MEGASync/mega/


%build
# configure sdk
cd %{_builddir}/%{buildsubdir}/src/MEGASync/mega
./autogen.sh
./configure \
    --disable-shared \
    --enable-static \
    --disable-silent-rules \
    --disable-curl-checks \
    --disable-examples \
    --with-cryptopp \
    --with-sodium \
    --with-zlib \
    --with-sqlite \
    --with-cares \
    --with-curl \
    --without-freeimage \
    --with-libuv \
    --with-libzen \
    --with-libmediainfo \
    --with-ffmpeg=/usr/include/ffmpeg \
    --prefix="%{_builddir}/%{buildsubdir}/src/MEGASync/mega/bindings/qt/3rdparty"

# build main package
cd %{_builddir}/%{buildsubdir}/src
%qmake_qt5
# --with-ffmpeg option doesn't work yet
sed -i 's/INCLUDEPATH.*/& \/usr\/include\/ffmpeg/g' MEGASync/MEGASync.pro
lrelease-qt5 MEGASync/MEGASync.pro
%make_build

# build nautilus extension
cd MEGAShellExtNautilus
%qmake_qt5
%make_build

# build nemo extension
cd ../MEGAShellExtNemo
%qmake_qt5
%make_build

%install
# install main package and missing binary
cd src
INSTALL_ROOT=%{buildroot} %make_install
cd MEGASync
install -Dm 755 megasync %{buildroot}/usr/bin/megasync
mkdir -p  %{buildroot}/etc/sysctl.d/
echo "fs.inotify.max_user_watches = 524288" > %{buildroot}/etc/sysctl.d/100-megasync-inotify-limit.conf

# install nautilus extension
cd ../MEGAShellExtNautilus
INSTALL_ROOT=%{buildroot} make install_target install_emblems32 install_emblems64
# install nautilus extension
cd ../MEGAShellExtNemo
INSTALL_ROOT=%{buildroot} make install_target install_emblems32 install_emblems64

%files
%license LICENCE.md
%{_datadir}/applications/megasync.desktop
%{_datadir}/icons/*
%{_datadir}/doc/megasync
%{_bindir}/megasync
%{_sysconfdir}/sysctl.d/100-megasync-inotify-limit.conf

%package nautilus
Requires: megasync
Requires: nautilus-extensions
Summary:  Nautilus extension for megasync
%description nautilus
Nautilus extension for megasync
%files nautilus
%{_libdir}/nautilus/extensions-3.0

%package nemo
Requires: megasync
Requires: nemo-extensions
Summary:  Nemo extension for megasync
%description nemo
Nemo extension for megasync
%files nemo
%{_libdir}/nemo/extensions-3.0

%changelog
* Thu Jun 13 2019 ThanosApostolou <thanosapostolou@outlook.com>
- update to 4.1.1.0

* Tue Apr  9 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial Package
