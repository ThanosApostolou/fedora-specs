Name:           birdtray
Version:        1.6
Release:        1%{?dist}
Summary:        a system tray new mail notification for Thunderbird
License:        GPLv3+
URL:            https://github.com/gyunaev/birdtray
Source0:        https://github.com/gyunaev/birdtray/archive/RELEASE_%{version}.tar.gz
Source1:        https://gitlab.com/ThanosApostolou/fedora-specs/raw/master/SOURCES/birdtray.desktop

BuildRequires:  libsqlite3x-devel
BuildRequires:  qt5-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  qt5-rpm-macros

%description
Birdtray is a system tray new mail notification for Thunderbird 60+
which does not require extensions.

%prep
%autosetup -n birdtray-RELEASE_%{version}


%build
mkdir build && cd build
%qmake_qt5 ../src
%make_build


%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/%{_datadir}/applications

install -m755 build/birdtray   %{buildroot}/%{_bindir}/birdtray
install -m644 %{SOURCE1} %{buildroot}/%{_datadir}/applications/birdtray.desktop

%files
%{_bindir}/birdtray
%{_datadir}/applications/birdtray.desktop



%changelog
* Tue May  21 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Update to 1.6, remove unneeded .o and .h files

* Sun Apr  7 2019 ThanosApostolou <thanosapostolou@outlook.com>
- Initial inclusion
