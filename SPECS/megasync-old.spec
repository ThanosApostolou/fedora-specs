Name:		megasync
Version:	4.0.2
Release:	1.5
Summary:	Easy automated syncing between your computers and your MEGA cloud drive
License:	Freeware
Group:		Applications/Others
Url:		https://mega.nz
Source0:	megasync_%{version}.tar.gz
Vendor:		MEGA Limited
Packager:	MEGA Linux Team <linux@mega.co.nz>

BuildRequires: openssl-devel, sqlite-devel, zlib-devel, autoconf, automake, libtool, gcc-c++
BuildRequires: hicolor-icon-theme, unzip, wget
BuildRequires: ffmpeg-devel
BuildRequires: compat-ffmpeg28-devel
BuildRequires: libcurl-devel
BuildRequires: pkgconf-pkg-config
BuildRequires: LibRaw-devel
BuildRequires: libzen-devel, libmediainfo-devel
BuildRequires: c-ares-devel, cryptopp-devel
BuildRequires: desktop-file-utils
BuildRequires: qt5-qtbase-devel qt5-qttools-devel, qt5-qtsvg-devel
BuildRequires: terminus-fonts, fontpackages-filesystem
Requires: cryptopp >= 5.6.5
Requires: qt5-qtbase >= 5.6, qt5-qtsvg


%description
Secure:
Your data is encrypted end to end. Nobody can intercept it while in storage or in transit.
Flexible:
Sync any folder from your PC to any folder in the cloud. Sync any number of folders in parallel.
Fast:
Take advantage of MEGA's high-powered infrastructure and multi-connection transfers.
Generous:
Store up to 50 GB for free!

%prep
%setup -q

%build

%define flag_cryptopp %{nil}
%define flag_libraw %{nil}
%define flag_disablemediainfo -i

%if 0%{?fedora_version}==19 || 0%{?fedora_version}==20 || 0%{?fedora_version}==23 || 0%{?fedora_version}==24 || 0%{?centos_version} || 0%{?scientificlinux_version} || 0%{?rhel_version} || ( 0%{?suse_version} && 0%{?sle_version} < 120300)
%define flag_disablemediainfo %{nil}
%endif


%if 0%{?centos_version} || 0%{?scientificlinux_version}
%define flag_cryptopp -q
%endif

%if ( %{_target_cpu} == "i586" && ( 0%{?sle_version} == 120200 || 0%{?sle_version} == 120300) )
%define flag_libraw -W
%endif


%if 0%{?rhel_version}
%define flag_cryptopp -q
%define flag_libraw -W

%endif

%define flag_cares %{nil}
%if 0%{?rhel_version}
%define flag_cares -e
%endif

%define flag_disablezlib %{nil}
%if 0%{?fedora_version} == 23
%define flag_disablezlib -z
%endif

%if 0%{?suse_version} > 1320
%define flag_cryptopp -q
%endif

export DESKTOP_DESTDIR=$RPM_BUILD_ROOT/usr

./configure %{flag_cryptopp} -g %{flag_disablezlib} %{flag_cares} %{flag_disablemediainfo} %{flag_libraw}

# Fedora uses system Crypto++ header files
%if 0%{?fedora}
rm -fr MEGASync/mega/bindings/qt/3rdparty/include/cryptopp
%endif

%if ( 0%{?fedora_version} && 0%{?fedora_version}<=27 ) || ( 0%{?centos_version} == 600 ) || ( 0%{?suse_version} && 0%{?suse_version} <= 1320 && !0%{?sle_version} ) || ( 0%{?sle_version} && 0%{?sle_version} <= 120200 )
%define extraqmake DEFINES+=MEGASYNC_DEPRECATED_OS
%else
%define extraqmake %{nil}
%endif

%if 0%{?suse_version} != 1230
%define fullreqs "CONFIG += FULLREQUIREMENTS"
%else
sed -i "s/USE_LIBRAW/NOT_USE_LIBRAW/" MEGASync/MEGASync.pro
%define fullreqs %{nil}
%endif


%if 0%{?fedora} || 0%{?sle_version} >= 120200 || 0%{?suse_version} > 1320

%if 0%{?fedora_version} >= 23 || 0%{?sle_version} >= 120200 || 0%{?suse_version} > 1320
qmake-qt5 %{fullreqs} DESTDIR=%{buildroot}%{_bindir} THE_RPM_BUILD_ROOT=%{buildroot} %{extraqmake}
lrelease-qt5  MEGASync/MEGASync.pro
%else
qmake-qt4 %{fullreqs} DESTDIR=%{buildroot}%{_bindir} THE_RPM_BUILD_ROOT=%{buildroot} %{extraqmake}
lrelease-qt4  MEGASync/MEGASync.pro
%endif
%else

%if 0%{?rhel_version} || 0%{?centos_version} || 0%{?scientificlinux_version}
qmake-qt4 %{fullreqs} DESTDIR=%{buildroot}%{_bindir} THE_RPM_BUILD_ROOT=%{buildroot} %{extraqmake}
lrelease-qt4  MEGASync/MEGASync.pro
%else
qmake %{fullreqs} DESTDIR=%{buildroot}%{_bindir} THE_RPM_BUILD_ROOT=%{buildroot} %{extraqmake}
lrelease MEGASync/MEGASync.pro
%endif

%endif

make

%install
make install DESTDIR=%{buildroot}%{_bindir}
#mkdir -p %{buildroot}%{_datadir}/applications
#%{__install} MEGAsync/platform/linux/data/megasync.desktop -D %{buildroot}%{_datadir}/applications

%if 0%{?suse_version}
%suse_update_desktop_file -n -i %{name} Network System
%else
desktop-file-install \
    --add-category="Network" \
    --dir %{buildroot}%{_datadir}/applications \
%{buildroot}%{_datadir}/applications/%{name}.desktop
%endif

mkdir -p  %{buildroot}/etc/sysctl.d/
echo "fs.inotify.max_user_watches = 524288" > %{buildroot}/etc/sysctl.d/100-megasync-inotify-limit.conf

%post
%if 0%{?suse_version} >= 1140
%desktop_database_post
%icon_theme_cache_post
%else
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/ubuntu-mono-dark &>/dev/null || :
%endif

%if 0%{?rhel_version} == 700
# RHEL 7
YUM_FILE="/etc/yum.repos.d/megasync.repo"
cat > "$YUM_FILE" << DATA
[MEGAsync]
name=MEGAsync
baseurl=https://mega.nz/linux/MEGAsync/RHEL_7/
gpgkey=https://mega.nz/linux/MEGAsync/RHEL_7/repodata/repomd.xml.key
gpgcheck=1
enabled=1
DATA
%endif

%if 0%{?scientificlinux_version} == 700
# Scientific Linux 7
YUM_FILE="/etc/yum.repos.d/megasync.repo"
cat > "$YUM_FILE" << DATA
[MEGAsync]
name=MEGAsync
baseurl=https://mega.nz/linux/MEGAsync/ScientificLinux_7/
gpgkey=https://mega.nz/linux/MEGAsync/ScientificLinux_7/repodata/repomd.xml.key
gpgcheck=1
enabled=1
DATA
%endif

%if 0%{?centos_version} == 700
# CentOS 7
YUM_FILE="/etc/yum.repos.d/megasync.repo"
cat > "$YUM_FILE" << DATA
[MEGAsync]
name=MEGAsync
baseurl=https://mega.nz/linux/MEGAsync/CentOS_7/
gpgkey=https://mega.nz/linux/MEGAsync/CentOS_7/repodata/repomd.xml.key
gpgcheck=1
enabled=1
DATA
%endif

%if 0%{?fedora}
YUM_FILE="/etc/yum.repos.d/megasync.repo"
cat > "$YUM_FILE" << DATA
[MEGAsync]
name=MEGAsync
baseurl=https://mega.nz/linux/MEGAsync/Fedora_\$releasever/
gpgkey=https://mega.nz/linux/MEGAsync/Fedora_\$releasever/repodata/repomd.xml.key
gpgcheck=1
enabled=1
DATA
%endif

%if 0%{?sle_version} == 120300
# openSUSE Leap 42.3
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_Leap_42.3/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_Leap_42.3/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif


%if 0%{?sle_version} == 120200
# openSUSE Leap 42.2
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_Leap_42.2/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_Leap_42.2/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif

%if 0%{?sle_version} == 120100
# openSUSE Leap 42.1
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_Leap_42.1/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_Leap_42.1/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif
 
%if 0%{?sle_version} == 150000  
# openSUSE Leap 15
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_Leap_15.0/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_Leap_15.0/repodata/repomd.xml.key
enabled=1
DATA
fi
%else
%if 0%{?suse_version} > 1320
# openSUSE Tumbleweed (rolling release)
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_Tumbleweed/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_Tumbleweed/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif
%endif

%if 0%{?suse_version} == 1320
# openSUSE 13.2
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_13.2/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_13.2/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif

%if 0%{?suse_version} == 1310
# openSUSE 13.1
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_13.1/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_13.1/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif

%if 0%{?suse_version} == 1230
# openSUSE 12.3
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_12.3/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_12.3/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif

%if 0%{?suse_version} == 1220
# openSUSE 12.2
if [ -d "/etc/zypp/repos.d/" ]; then
ZYPP_FILE="/etc/zypp/repos.d/megasync.repo"
cat > "$ZYPP_FILE" << DATA
[MEGAsync]
name=MEGAsync
type=rpm-md
baseurl=https://mega.nz/linux/MEGAsync/openSUSE_12.2/
gpgcheck=1
autorefresh=1
gpgkey=https://mega.nz/linux/MEGAsync/openSUSE_12.2/repodata/repomd.xml.key
enabled=1
DATA
fi
%endif


### include public signing key #####
# Install new key if it's not present
# Notice, for openSuse, postinst is checked (and therefore executed) when creating the rpm
# we need to ensure no command results in fail (returns !=0)
rpm -q gpg-pubkey-7f068e5d-563dc081 > /dev/null 2>&1 || KEY_NOT_FOUND=1

if [ ! -z "$KEY_NOT_FOUND" ]; then
KEYFILE=$(mktemp /tmp/megasync.XXXXXX || :)
if [ -n "$KEYFILE" ]; then
    cat > "$KEYFILE" <<KEY || :
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2

mI0EVj3AgQEEAO2XyJgpvE5HDRVsggcrMhf5+KpQepl7m7OyrPSgxLi72Wuy5GWp
hO64BX1UzmdUirIEOc13YxdeuhwJ3YP0wnKUyUrdWA0r2HjOz555vN6ldrPlSCBI
RxKBWRMQaR4cwNKQ8V4xV9tVdPGgrQ9L/4H3fM9fYqCwEMKBxxLZsF3PABEBAAG0
IE1lZ2FMaW1pdGVkIDxzdXBwb3J0QG1lZ2EuY28ubno+iL8EEwECACkFAlY9wIEC
GwMFCRLMAwAHCwkIBwMCAQYVCAIJCgsEFgIDAQIeAQIXgAAKCRADw606fwaOXfOS
A/998rh6f0wsrHmX2LTw2qmrWzwPj4m+vp0m3w5swPZw1x4qSNsmNsIXX8J0ZcSE
qymOwHZ0B9imBS3iz+U496NSfPNWABbIBnUAu8zq0IR28Q9pUcLe5MWFsw9NO+w2
5dByoN9JKeUftZt1x76NHn5wmxB9fv7WVlCnZJ+T16+nh7iNBFY9wIEBBADHpopM
oXNkrGZLI6Ok1F5N7+bSgiyZwkvBMAqCkPawUgwJztFKGf8F/sSbydsKRC2aQcuJ
eOj0ZPUtJ80+o3w8MsHRtZDSxDIxqqiHeupoDRI3Be9S544vI5/UmiiygTuhmNTT
NWgStoZz7hEK4IsELAG1EFodIMtBSkptDL92HwARAQABiKUEGAECAA8FAlY9wIEC
GwwFCRLMAwAACgkQA8OtOn8Gjl3HlAQAoOckF5JBJWekmlX+k2RYwtgfszk31Gq+
Jjiho4rUEW8c1EUPvK8v1jRGwjYED3ihJ6510eblYFPl+6k91OWlScnxuVVAmSn4
35RW3vR+nYUvf3s8rctbw97gJJZAA7p5oAowTux3oHotKSYhhxKcz15goMXzSb5G
/h7fJRhMnw4=
=fp/e
-----END PGP PUBLIC KEY BLOCK-----
KEY

mv /var/lib/rpm/.rpm.lock /var/lib/rpm/.rpm.lock_moved || : #to allow rpm import within postinst
%if 0%{?suse_version}
#Key import would hang and fail due to lock in /var/lib/rpm/Packages. We create a copy
mv /var/lib/rpm/Packages{,_moved}
cp /var/lib/rpm/Packages{_moved,}
%endif
rpm --import "$KEYFILE" 2>&1 || FAILED_IMPORT=1
%if 0%{?suse_version}
rm /var/lib/rpm/Packages_moved  #remove the old one
%endif
mv /var/lib/rpm/.rpm.lock_moved /var/lib/rpm/.rpm.lock || : #take it back

rm $KEYFILE || :
fi
fi

sysctl -p /etc/sysctl.d/100-megasync-inotify-limit.conf

### END of POSTINST


%postun
%if 0%{?suse_version} >= 1140
%desktop_database_postun
%icon_theme_cache_postun
%else
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
    /bin/touch --no-create %{_datadir}/icons/ubuntu-mono-dark &>/dev/null || :
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/* &>/dev/null || :
fi
%endif
# kill running MEGAsync instance
killall megasync 2> /dev/null || true


%if 0%{?fedora} || 0%{?rhel_version} || 0%{?centos_version} || 0%{?scientificlinux_version}
%posttrans
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/ubuntu-mono-dark &>/dev/null || :
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/* &>/dev/null || :
%endif

%clean
%{?buildroot:%__rm -rf "%{buildroot}"}

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%{_datadir}/applications/megasync.desktop
%{_datadir}/icons/hicolor/*/*/mega.png
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/icons/*/*/*/*
%{_datadir}/doc/megasync
%{_datadir}/doc/megasync/*
/etc/sysctl.d/100-megasync-inotify-limit.conf

%changelog
* Mon Feb  4 2019 linux@mega.co.nz
- Update to version 4.0.2:
  * Fix bug with selection of transfer manager items
  * Fix bug of context menu not shown over transfer manager items
  * New design for the main dialog
  * Improved setup assistant
  * Support to show Public Service Announcements
  * Modern notifications
  * Updated third-party libraries
  * Other minor bug fixes and improvements
* Wed Sep 12 2018 linux@mega.co.nz
- Update to version 3.7.1:
  * Support for multi-factor authentication
  * Security improvements for the registration of new accounts
  * Better performance for the upload of images
  * Creation of thumbnails and previews for RAW images
  * Improvements in the management of network connections
  * New logic to get external changes in the MEGA account
  * Other minor bug fixes and improvements
* Wed May 23 2018 linux@mega.co.nz
- Update to version 3.6.6:
  * General Data Protection Regulation (GDPR) compliance
  * Updated translations
  * Bug fixes and other minor adjustments
* Wed May  2 2018 linux@mega.co.nz
- Update to version 3.6.5:
  * Allow to change the password
  * Improvements for the sync engine
  * Updated third-party libraries
  * Bug fixes and other minor adjustments
* Fri Jan 26 2018 linux@mega.co.nz
- Update to version 3.6.0:
  * New options related to file versioning
  * Local backup cleaning scheduler
  * Support for video thumbnails and metadata
  * Bug fixes and other minor improvements
* Wed Dec 13 2017 linux@mega.co.nz
- Update to version 3.5.3:
  * Integration with Finder (macOS 10.10+)
  * Faster scanning of files
  * Improved the management of deleted files
  * Bug fixes and other minor improvements
* Tue Nov  7 2017 linux@mega.co.nz
- Update to version 3.5:
  * New UI style
  * Support to generate file versions in MEGA
  * Integration with Finder (macOS 10.10+)
  * Allow to exclude specific files/folders
  * Bug fixes and other minor improvements
* Fri Jun 23 2017 linux@mega.co.nz
- Update to version 3.1.4:
  * Support for Apple File System (macOS High Sierra)
  * Allow to disable left pane icons in settings (Windows 10)
  * Updated translations
  * Bug fixes
* Fri May 12 2017 linux@mega.co.nz
- Update to version 3.1.2:
  * Better integration with Windows 10
  * Updated SSL certificate for communications with the web
  * Deprecated OS X versions prior to Mavericks
  * Bug fixes
* Mon Feb  6 2017 linux@mega.co.nz
- Update to version 3.0.1:
  * Transfer manager
  * Transfer speed control
  * Configurable number of connections per transfer
  * Improved the transfer speed
  * Reduced CPU usage during synchronization
  * Bug fixes
* Fri Nov 11 2016 linux@mega.co.nz
- Update to version 2.9.10:
  * Support to download/import folder links
  * Automatic HTTP/HTTPS proxy detection on OS X
  * Better compatibility with antivirus software
  * HDPI support for Linux distros having QT >= 5.6
  * Other bug fixes and minor improvements
* Mon Oct 17 2016 linux@mega.co.nz
- Update to version 2.9.9:
  * Fixed compatibility with some web browsers
  * Fixed the creation of thumbnails for JPG images (Windows)
  * Fixed incompatibilities with some WiFi drivers (Windows)
  * Fixed problems downloading files with incorrect metadata
  * Other bug fixes
* Wed Jul 20 2016 linux@mega.co.nz
- Update to version 2.9.8:
  * More efficient upload completion
  * Creation of new cryptographic keys
  * Upgrade to QT5 and VS2015 (Windows)
  * Bug fixes
* Tue May 31 2016 linux@mega.co.nz
- Update to version 2.9.5:
  * Save the state and resume transfers
  * Better management of errors during transfers
  * Compatibility with HDPI displays
  * Bug fixes
* Mon Apr 11 2016 linux@mega.co.nz
- Update to version 2.9.0:
  * Management of bandwidth quota for PRO users
  * New information dialog
  * ARM support (Raspberry Pi)
  * Bug fixes
* Wed Apr  6 2016 linux@mega.co.nz
- Update to version 2.8.0:
  * Check if public links have been already downloaded
  * Better management of communications with the webclient
  * Management of bandwidth quotas
  * Bug fixes
* Tue Mar  1 2016 linux@mega.co.nz
- Update to version 2.7.2:
  * Fixed the compilation. The previous version failed with some CPUs
* Sun Feb 21 2016 linux@mega.co.nz
- Update to version 2.7.1:
  * Allow to select hidden files and folders
  * Management of permissions (OS X, Linux)
  * Better management of HTTPS errors
  * Allow to use HTTPS connections only
  * Bug fixes
* Tue Jan 19 2016 linux@mega.co.nz
- Update to version 2.6.1:
  * Streaming from MEGA with a builtin HTTP proxy server
  * Fixed the automatic detection of the proxy (Win32)
  * More and better translations
  * Bug fixes
* Mon Jan  4 2016 linux@mega.co.nz
- Update to version 2.5.3:
  * Allow the usage of MEGAsync without being logged in
  * New network layer using cURL and c-ares (Win32)
  * Accessibility and SOCKS5 proxy support (Win32)
  * Bug fixes and improvements for the synchronization engine
* Thu Oct  8 2015 linux@mega.co.nz
- Update to version 2.3.1:
  * Inform about MITM attacks
  * Compatibility with OS X 10.11 El Capita
  * Improvements for the synchronization engine
  * Bug fixes
* Mon Aug 10 2015 linux@mega.co.nz
- Update to version 2.1.1:
  * Built with a custom version of QT that will allow us to debug crashes in QT libraries
  * List of allowed origins for webclient communications
  * Disable the crash handler when MEGAsync is being closed (to ignore crashes closing QT when the app exits)
  * Report events for new installations
  * Built with a custom version of QT that will allow us to debug crashes in QT libraries
  * List of allowed origins for webclient communications
  * Disable the crash handler when MEGAsync is being closed (to ignore crashes closing QT when the app exits)
  * Report events for new installations
  * Built with a custom version of QT that will allow us to debug crashes in QT libraries
  * List of allowed origins for webclient communications
  * Disable the crash handler when MEGAsync is being closed (to ignore crashes closing QT when the app exits)
  * Report events for new installations
  * Built with a custom version of QT that will allow us to debug crashes in QT libraries
  * List of allowed origins for webclient communications
  * Disable the crash handler when MEGAsync is being closed (to ignore crashes closing QT when the app exits)
  * Report events for new installations
