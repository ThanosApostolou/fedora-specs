Name:           vertex-theme
Version:        20180519
Release:        1%{?dist}
Summary:        Theme for GTK 3, GTK 2, Gnome-Shell and Cinnamon
License:        GPLv3
URL:            https://github.com/oberon-manjaro/%{name}
Source0:        https://github.com/oberon-manjaro/%{name}/archive/%{version}.tar.gz

BuildArch:      noarch

Requires:       filesystem
Requires:       gnome-themes-standard
Requires:       gtk-murrine-engine
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  pkgconf
BuildRequires:  gtk3-devel

%description 
Vertex is a theme for GTK 3, GTK 2, Gnome-Shell and Cinnamon. It supports GTK 3
and GTK 2 based desktop environments, like Gnome, Cinnamon, Mate, XFCE, Budgie,
Pantheon, etc. Themes for the Browsers Chrome/Chromium and Firefox are included
too. The theme comes with three variants to choose from. The default variant
with dark header-bars, a light variant, and a dark variant

%prep
%autosetup

%build
./autogen.sh --prefix=%{_prefix} --with-gnome=3.22

%install
%make_install

%files
%license COPYING
%doc README.md
%{_datadir}/themes/Vertex/
%{_datadir}/themes/Vertex-Dark/
%{_datadir}/themes/Vertex-Light/

%changelog
* Sun Apr 7 2019 Thanos Apostolou - 20180519-1
- Initial inclusion from oberon repository

